module github.com/mdouchement/checksum

go 1.18

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.4.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
)
